<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css">
</head>

<body>

<div id="header">
<a href=".">
<div class="title">
Mille Mots
</div>
</a>
<div class="menu">
<a rel="me" href="https://mamot.fr/@francoisremi" target="_blank"><img src="images/masto.png" height="18em"/></a>
<a href="mailto:contact@francoisremi.fr"><img src="images/mail.png" height="15em"/></a>
</div>
</div>

<div id="main">
<?php

if(isset($_GET['n'])) {
    echo('<div class="texte">');
    include("textes/".$_GET['n'].".html");
    echo('</div>');
}
else{
?>
<ul class="textes">
<a href="?n=distopsia"><li>Distopsia</li></a>
</ul>
<?php
}
?>

</div>

</body>
</html>
